﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FootballLeague.Data.DBModels;
using FootballLeague.Services.Contracts;
using FootballLeague.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FootballLeague.Web.Controllers
{
    public class MatchesController : Controller
    {

        private readonly IService service;

        public MatchesController(IService service)
        {
            this.service = service;
        }
        // GET: MatchesController
        public async Task<ActionResult> Index()
        {
            try
            {
                var matches = await this.service.ServeCommand("GetMatches", null) as List<Match>;
                if (matches == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return View(matches);
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }

        }

        // GET: MatchesController/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            try
            {
                var match = await this.service.ServeCommand("GetMatch", id) as Match;
                if (match == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return View(match);
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
        }

        // GET: MatchesController/Create
        public async Task<ActionResult> Create()
        {
            try
            {
                var teams = await this.service.ServeCommand("GetTeams", null) as List<Team>;
                if (teams == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                ViewData["Teams"] = teams.Select(x => new SelectListItem(x.Name, x.Id.ToString()));
                return View();
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }   
        }

        // POST: MatchesController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Match collection)
        {
            try
            {
                var match = await this.service.ServeCommand("CreateMatch", collection) as Match;
                if (match == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return RedirectToAction(nameof(Details), "Matches", new { id = match.Id });
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
        }

        // GET: MatchesController/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            try
            {
                var match = await this.service.ServeCommand("GetMatch", id) as Match;
                if (match == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                var teams = await this.service.ServeCommand("GetTeams", null) as List<Team>;
                if (teams == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                ViewData["Teams"] = teams.Select(x => new SelectListItem(x.Name, x.Id.ToString()));
                return View(match);
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
            
        }

        // POST: MatchesController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Guid id, Match collection)
        {
            try
            {
                var match = await this.service.ServeCommand("EditMatch", new Tuple<Guid, Match>(id, collection)) as Match;
                if (match == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return RedirectToAction(nameof(Details), "Matches", new { id = match.Id });
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
        }

        // GET: MatchesController/Delete/5
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                var match = await this.service.ServeCommand("GetMatch", id) as Match;
                if (match == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return View(match);
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
            
        }

        // POST: MatchesController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(Guid id, Match collection)
        {
            try
            {
                var success = await this.service.ServeCommand("DeleteMatch", id) as bool?;
                if (success != null && success == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                throw new ArgumentNullException("Operation failed");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
        }
    }
}
