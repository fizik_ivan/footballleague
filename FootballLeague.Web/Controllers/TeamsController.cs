﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FootballLeague.Data.DBModels;
using FootballLeague.Services.Contracts;
using FootballLeague.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FootballLeague.Web.Controllers
{
    public class TeamsController : Controller
    {
        private readonly IService service;
        public TeamsController(IService service)
        {
            this.service = service;
        }
        // GET: TeamsController
        public async Task<ActionResult> Index()
        {
            try
            {
                var teams = await this.service.ServeCommand("GetTeams", null) as List<Team>;
                if (teams == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return View(teams);
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
            
        }

        // GET: TeamsController/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            try
            {
                var team = await this.service.ServeCommand("GetTeam", id) as Team;
                if (team == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return View(team);
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
            
        }

        // GET: TeamsController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TeamsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Team collection)
        {
            try
            {

                var team = await this.service.ServeCommand("CreateTeam", collection) as Team;
                if (team == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return RedirectToAction(nameof(Details), "Teams", new { id = team.Id });
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Home", new { message = e.Message }) ;
            }
        }

        // GET: TeamsController/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            try
            {
                var team = await this.service.ServeCommand("GetTeam", id) as Team;
                if (team == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return View(team);
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
        }

        // POST: TeamsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Guid id, Team collection)
        {
            try
            {
                var team = await this.service.ServeCommand("EditTeam", new Tuple<Guid, Team>(id, collection)) as Team;
                if (team == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return RedirectToAction(nameof(Details), "Teams", new { id = team.Id });
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
        }

        // GET: TeamsController/Delete/5
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                var team = await this.service.ServeCommand("GetTeam", id) as Team;
                if (team == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                return View(team);
            }
            catch (Exception e)
            {

                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
            
        }

        // POST: TeamsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(Guid id, Team collection)
        {
            try
            {
                var matches = await this.service.ServeCommand("GetMatches", null) as List<Match>;
                if (matches == null)
                {
                    throw new ArgumentNullException("Operation failed");
                }
                var success = await this.service.ServeCommand("DeleteTeam", new Tuple<Guid, List<Match>>(id, matches)) as bool?;
                if (success != null && success == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                throw new ArgumentNullException("Operation failed");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Home", new { message = e.Message });
            }
        }
    }
}
