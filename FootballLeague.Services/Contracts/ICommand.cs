﻿using FootballLeague.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Contracts
{
    internal interface ICommand
    {
        Task<object> Execute(object parameters, FLContext context);
    }
}
