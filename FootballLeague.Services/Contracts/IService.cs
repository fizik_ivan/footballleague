﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Contracts
{
    public interface IService
    {
        public Task<object> ServeCommand(string command, object parameters);
    }
}
