﻿using FootballLeague.Data;
using FootballLeague.Services.Contracts;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace FootballLeague.Services
{
    public class Service : IService
    {
        private readonly FLContext context;

        public Service(FLContext context)
        {
            this.context = context;
        }

        public async Task<object> ServeCommand(string commandName, object parameters)
        {
            var commandTypeInfo = this.FindCommand(commandName);
            var command = Activator.CreateInstance(commandTypeInfo) as ICommand;
            try
            {
                return await command.Execute(parameters, context);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private TypeInfo FindCommand(string commandName)
        {
            Assembly currentAssembly = this.GetType().GetTypeInfo().Assembly;
            var commandTypeInfo = currentAssembly.DefinedTypes
                .Where(type => type.ImplementedInterfaces.Any(inter => inter == typeof(ICommand)))
                .Where(type => type.Name.ToLower() == (commandName.ToLower() + "command"))
                .SingleOrDefault();

            if (commandTypeInfo == null)
            {
                throw new ArgumentException("The passed command is not found!");
            }

            return commandTypeInfo;
        }
    }
}
