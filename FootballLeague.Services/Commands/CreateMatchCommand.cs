﻿using FootballLeague.Data;
using FootballLeague.Data.DBModels;
using FootballLeague.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class CreateMatchCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            try
            {
                var temp = parameters as Match;
                if (temp == null)
                {
                    throw new ArgumentNullException("No information for creating the match");
                }
                if (temp.HomeTeamId == Guid.Empty || temp.AwayTeamId == Guid.Empty)
                {
                    throw new ArgumentException("No team provided");
                }
                if (temp.HomeTeamGoals < 0 || temp.AwayTeamGoals < 0)
                {
                    throw new ArgumentException("Goals can't be negative");
                }
                if (!await Exists(context, temp))
                {
                    var match = new Match()
                    {
                        Date = temp.Date,
                        HomeTeamId = temp.HomeTeamId,
                        HomeTeam = await context.Teams.FindAsync(temp.HomeTeamId),
                        AwayTeamId = temp.AwayTeamId,
                        AwayTeam = await context.Teams.FindAsync(temp.AwayTeamId),
                        HomeTeamGoals = temp.HomeTeamGoals,
                        AwayTeamGoals = temp.AwayTeamGoals
                    };
                    await context.Matches.AddAsync(match);
                    UpdateTeamScores(match, context);
                    await context.SaveChangesAsync();
                    return await context.Matches.Include(m => m.HomeTeam)
                                                .Include(m => m.AwayTeam)
                                                .FirstOrDefaultAsync(m => m.HomeTeamId == match.HomeTeamId &&
                                                                    m.AwayTeamId == match.AwayTeamId &&
                                                                    m.Date.Date.Equals(match.Date.Date));
                }
                throw new InvalidOperationException("Team already exist");
            }
            catch (Exception)
            {
                throw new Exception("Creation and recording of team failed");
            }

        }

        private async Task<bool> Exists(FLContext context, Match match)
        {
            if (await context.Matches.FirstOrDefaultAsync(m => m.HomeTeamId == match.HomeTeamId &&
                                                                m.AwayTeamId == match.AwayTeamId &&
                                                                m.Date.Date.Equals(match.Date.Date)) != null)
            {
                return true;
            }
            return false;
        }

        private async void UpdateTeamScores(Match match, FLContext context)
        {
            if (match.HomeTeamGoals == match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamHome.Score++;
                teamAway.Score++;
                context.Update(teamHome);
                context.Update(teamAway);
            }
            else if (match.HomeTeamGoals > match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                teamHome.Score += 3;
                context.Update(teamHome);
            }
            else
            {
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamAway.Score += 3;
                context.Update(teamAway);
            }

        }
    }
}
