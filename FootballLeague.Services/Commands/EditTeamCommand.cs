﻿using FootballLeague.Data;
using FootballLeague.Data.DBModels;
using FootballLeague.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class EditTeamCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            try
            {
                var temp = parameters as Tuple<Guid,Team>;
                if (temp == null)
                {
                    throw new ArgumentNullException("No information for updating the team");
                }
                if (temp.Item1 == Guid.Empty)
                {
                    throw new InvalidOperationException("Team does not exist");
                }
                if (temp.Item2 == null)
                {
                    throw new ArgumentNullException("No information for updating the team");
                }
                if (String.IsNullOrEmpty(temp.Item2.Name))
                {
                    throw new ArgumentException("No name provided");
                }
                if (temp.Item2.Score < 0)
                {
                    throw new ArgumentException("Score can't be negative");
                }
                var team = await context.Teams.FindAsync(temp.Item1);
                team.Name = temp.Item2.Name;
                team.Score = temp.Item2.Score;

                context.Update(team);
                await context.SaveChangesAsync();
                return team;
            }
            catch (Exception)
            {
                throw new Exception("Updating and recording of team failed");
            }

        }
    }
}
