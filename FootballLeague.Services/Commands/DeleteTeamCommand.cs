﻿using FootballLeague.Data;
using FootballLeague.Data.DBModels;
using FootballLeague.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class DeleteTeamCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            try
            {
                var temp = parameters as Tuple<Guid, List<Match>>;
                if (temp == null)
                {
                    throw new ArgumentNullException("No information for deleting the team");
                }
                if (temp.Item1 == Guid.Empty)
                {
                    throw new ArgumentException("No information for deleting the team");
                }
                if (temp.Item2 == null)
                {
                    throw new ArgumentNullException("No information for assotiated matches");
                }
                var relevantMatches = temp.Item2.Where(m => m.HomeTeamId == temp.Item1 || m.AwayTeamId == temp.Item1).ToList();
                foreach (var item in relevantMatches)
                {
                    SubstractScore(context, item);
                    context.Matches.Remove(item);
                }
                var team = await context.Teams.FindAsync(temp.Item1);
                context.Teams.Remove(team);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        private async void SubstractScore(FLContext context, Match match)
        {
            if (match.HomeTeamGoals == match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamHome.Score--;
                teamAway.Score--;
                context.Update(teamHome);
                context.Update(teamAway);
            }
            else if (match.HomeTeamGoals > match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                teamHome.Score -= 3;
                context.Update(teamHome);
            }
            else
            {
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamAway.Score -= 3;
                context.Update(teamAway);
            }
        }
    }
}
