﻿using FootballLeague.Data;
using FootballLeague.Data.DBModels;
using FootballLeague.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class EditMatchCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            try
            {
                var temp = parameters as Tuple<Guid, Match>;
                if (temp == null)
                {
                    throw new ArgumentNullException("No information for updating the match");
                }
                if (temp.Item1 == Guid.Empty)
                {
                    throw new InvalidOperationException("Match does not exist");
                }
                if (temp.Item2 == null)
                {
                    throw new ArgumentNullException("No information for updating the match");
                }
                if (temp.Item2.HomeTeamId == Guid.Empty || temp.Item2.AwayTeamId == Guid.Empty)
                {
                    throw new ArgumentException("No team provided");
                }
                if (await context.Teams.FindAsync(temp.Item2.HomeTeamId) == null ||
                    await context.Teams.FindAsync(temp.Item2.AwayTeamId) == null)
                {
                    throw new ArgumentException("Team does not exist");
                }
                if (temp.Item2.HomeTeamGoals < 0 || temp.Item2.AwayTeamGoals < 0)
                {
                    throw new ArgumentException("Goals can't be negative");
                }
                if (temp.Item2.Date == default)
                {
                    throw new ArgumentException("No date provided");
                }
                var match = await context.Matches.Include(m => m.HomeTeam)
                                                    .Include(m => m.AwayTeam)
                                                    .FirstOrDefaultAsync(m => m.Id == temp.Item1);
                SubstractScore(context, match);
                match.Date = temp.Item2.Date;
                match.HomeTeamId = temp.Item2.HomeTeamId;
                match.HomeTeam = await context.Teams.FindAsync(temp.Item2.HomeTeamId);
                match.AwayTeamId = temp.Item2.AwayTeamId;
                match.AwayTeam = await context.Teams.FindAsync(temp.Item2.AwayTeamId);
                match.HomeTeamGoals = temp.Item2.HomeTeamGoals;
                match.AwayTeamGoals = temp.Item2.AwayTeamGoals;
                AddScore(context, match);
                context.Update(match);
                await context.SaveChangesAsync();
                return match;
            }
            catch (Exception)
            {
                throw new Exception("Updating and recording of team failed");
            }
        }

        private async void SubstractScore(FLContext context, Match match)
        {
            if (match.HomeTeamGoals == match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamHome.Score--;
                teamAway.Score--;
                context.Update(teamHome);
                context.Update(teamAway);
            }
            else if (match.HomeTeamGoals > match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                teamHome.Score -= 3;
                context.Update(teamHome);
            }
            else
            {
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamAway.Score -= 3;
                context.Update(teamAway);
            }
        }

        private async void AddScore(FLContext context, Match match)
        {
            if (match.HomeTeamGoals == match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamHome.Score++;
                teamAway.Score++;
                context.Update(teamHome);
                context.Update(teamAway);
            }
            else if (match.HomeTeamGoals > match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                teamHome.Score += 3;
                context.Update(teamHome);
            }
            else
            {
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamAway.Score += 3;
                context.Update(teamAway);
            }
        }
    }
}
