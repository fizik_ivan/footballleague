﻿using FootballLeague.Data;
using FootballLeague.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class GetTeamsCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            return await context.Teams.OrderByDescending(s => s.Score).ToListAsync();
        }
    }
}
