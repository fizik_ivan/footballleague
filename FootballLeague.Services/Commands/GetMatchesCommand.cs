﻿using FootballLeague.Data;
using FootballLeague.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class GetMatchesCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            return await context.Matches.Include(m => m.HomeTeam)
                                        .Include(m => m.AwayTeam)
                                        .OrderByDescending(s => s.Date)
                                        .ToListAsync();
        }
    }
}
