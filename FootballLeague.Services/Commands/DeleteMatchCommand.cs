﻿using FootballLeague.Data;
using FootballLeague.Data.DBModels;
using FootballLeague.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class DeleteMatchCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            try
            {
                var matchId = parameters as Guid?;
                if (matchId == null || matchId == Guid.Empty)
                {
                    throw new ArgumentNullException("No information for deleting the match");
                }
                var match = await context.Matches.Include(m => m.HomeTeam)
                                            .Include(m => m.AwayTeam)
                                            .FirstOrDefaultAsync(m => m.Id == matchId);
                SubstractScore(context, match);
                context.Matches.Remove(match);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        private async void SubstractScore(FLContext context, Match match)
        {
            if (match.HomeTeamGoals == match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamHome.Score--;
                teamAway.Score--;
                context.Update(teamHome);
                context.Update(teamAway);
            }
            else if (match.HomeTeamGoals > match.AwayTeamGoals)
            {
                var teamHome = await context.Teams.FindAsync(match.HomeTeamId);
                teamHome.Score -= 3;
                context.Update(teamHome);
            }
            else
            {
                var teamAway = await context.Teams.FindAsync(match.AwayTeamId);
                teamAway.Score -= 3;
                context.Update(teamAway);
            }
        }
    }
}
