﻿using FootballLeague.Data;
using FootballLeague.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class GetTeamCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            var team = parameters as Guid?;
            if (team == null || team == Guid.Empty)
            {
                throw new ArgumentNullException("No information for retrieving the team");
            }
            return await context.Teams.FindAsync(team);
        }
    }
}
