﻿using FootballLeague.Data;
using FootballLeague.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class GetMatchCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            var match = parameters as Guid?;
            if (match == null || match == Guid.Empty)
            {
                throw new ArgumentNullException("No information for retrieving the match");
            }
            return await context.Matches.Include(m => m.HomeTeam)
                                        .Include(m => m.AwayTeam)
                                        .FirstOrDefaultAsync(m => m.Id == match);
        }
    }
}
