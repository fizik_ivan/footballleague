﻿using FootballLeague.Data;
using FootballLeague.Data.DBModels;
using FootballLeague.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLeague.Services.Commands
{
    public class CreateTeamCommand : ICommand
    {
        public async Task<object> Execute(object parameters, FLContext context)
        {
            try
            {
                var temp = parameters as Team;
                if (temp == null)
                {
                    throw new ArgumentNullException("No information for creating the team");
                }
                if (String.IsNullOrEmpty(temp.Name))
                {
                    throw new ArgumentException("No name provided");
                }
                if (temp.Score < 0)
                {
                    throw new ArgumentException("Score can't be negative");
                }
                if (!await Exists(context, temp.Name))
                {
                    var team = new Team() { Name = temp.Name, Score = temp.Score };
                    await context.Teams.AddAsync(team);
                    await context.SaveChangesAsync();
                    return await context.Teams.FirstOrDefaultAsync(t => t.Name.ToLower() == team.Name.ToLower());
                }
                throw new InvalidOperationException("Team already exist");
            }
            catch (Exception)
            {
                throw new Exception("Creation and recording of team failed");
            }

        }

        private async Task<bool> Exists(FLContext context, string name)
        {
            if (await context.Teams.FirstOrDefaultAsync(t => t.Name.ToLower() == name.ToLower()) != null)
            {
                return true;
            }
            return false;
        }
    }
}
