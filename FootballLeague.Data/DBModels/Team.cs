﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.DBModels
{
    public class Team
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Score { get; set; }
    }
}
