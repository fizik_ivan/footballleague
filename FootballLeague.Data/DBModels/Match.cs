﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.DBModels
{
    public class Match
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }
 
        public Guid HomeTeamId { get; set; }

        public Team HomeTeam { get; set; }

        public Guid AwayTeamId { get; set; }

        public Team AwayTeam { get; set; }

        public int HomeTeamGoals { get; set; }

        public int AwayTeamGoals { get; set; }
    }
}
