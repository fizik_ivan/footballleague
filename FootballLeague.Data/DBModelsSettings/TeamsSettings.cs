﻿using FootballLeague.Data.DBModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.DBModelsSettings
{
    internal class TeamsSettings : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.HasKey(k => k.Id);
            builder.HasIndex(p => p.Name).IsUnique();
            builder.Property(p => p.Score).IsRequired();
        }
    }
}
