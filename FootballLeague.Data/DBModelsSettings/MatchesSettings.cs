﻿using FootballLeague.Data.DBModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.DBModelsSettings
{
    internal class MatchesSettings : IEntityTypeConfiguration<Match>
    {
        public void Configure(EntityTypeBuilder<Match> builder)
        {
            //builder.HasIndex(k => new { k.Date, k.HomeTeamId, k.AwayTeamId});
            builder.HasKey(i => i.Id);
            builder.Property(p => p.HomeTeamGoals).IsRequired();
            builder.Property(p => p.AwayTeamGoals).IsRequired();
        }
    }
}
