﻿using FootballLeague.Data.DBModels;
using FootballLeague.Data.DBModelsSettings;
using Microsoft.EntityFrameworkCore;
using System;

namespace FootballLeague.Data
{
    public class FLContext : DbContext
    {
        public FLContext(DbContextOptions<FLContext> options) :
            base(options)
        {

        }

        public DbSet<Team> Teams { get; set; }

        public DbSet<Match> Matches { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new TeamsSettings());
            builder.ApplyConfiguration(new MatchesSettings());

            base.OnModelCreating(builder);

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
    }
}
